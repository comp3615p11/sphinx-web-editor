import os
import functools

from django.conf import settings

def temp_settings(**kwargs):
    """Decorator to temporarily modify django settings."""
    saved = {}
    def _modify_settings():
        for arg, val in kwargs.items():
            saved[arg] = getattr(settings, arg)
            setattr(settings, arg, val)
    def _restore_settings():
        for arg, val in saved.items():
            setattr(settings, arg, val)

    def wrap(test_func):
        @functools.wraps(test_func)
        def wrapper(*args, **kwargs):
            _modify_settings()
            test_func(*args, **kwargs)
            _restore_settings()

        return wrapper

    return wrap

def reauthoring_settings(test_func):
    base_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)))

    return temp_settings(
        # Use a test directory for the sphinx project
        REAUTHORING_SITE_DIR = os.path.join(base_dir, 'test-site/'),
        REAUTHORING_BUILD_DIR = os.path.join(base_dir, 'test-site/.build'),
        # Stop pipeline from packaging files
        STATICFILES_STORAGE = 'pipeline.storage.NonPackagingPipelineStorage'
    )(test_func)
