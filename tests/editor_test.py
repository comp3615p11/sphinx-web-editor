import os
import shutil

from django.conf import settings

from support import reauthoring_settings, temp_settings

@reauthoring_settings
def test_editor_for_public(client):
    response = client.get('/index.rst')

    assert response.status_code == 302

@reauthoring_settings
def test_editor(admin_client):
    response = admin_client.get('/index.rst')

    assert response.status_code == 200
    print(response.content)
    assert "Test documentation master file" in str(response.content)

@temp_settings(REAUTHORING_PUBLISH_SCRIPT="true")
def test_publish(admin_client):
    response = admin_client.post('/publish')

    assert response.status_code == 302
