import os

from django.conf import settings

from editor import sphinxwrapper
from support import reauthoring_settings

@reauthoring_settings
def test_sphinxwrapper():
    app, out, err = sphinxwrapper.build(settings.REAUTHORING_SITE_DIR, settings.REAUTHORING_BUILD_DIR, force_all=True)
    assert app.statuscode == 0
    assert out
    assert not err
    assert os.path.isfile(os.path.join(settings.REAUTHORING_BUILD_DIR, 'index.html'))
