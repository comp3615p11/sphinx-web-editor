import os

from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.views import View
from django import forms
from django.conf import settings

from django.contrib.auth.mixins import LoginRequiredMixin

from editor import sphinxwrapper

class ContentsForm(forms.Form):
    contents = forms.CharField()

class EditorView(LoginRequiredMixin, View):
    # TODO: Handle missing files gracefully

    def get(self, request):
        file = self._get_file(request, 'r')

        return render(request, 'editor.html', { 'contents': file.read() })

    def post(self, request):
        form = ContentsForm(request.POST)
        assert form.is_valid()

        with self._get_file(request, 'w') as file:
            file.write(form.cleaned_data['contents'])

        # Rebuild this file
        app, status, err = self._build_file(request)
        if settings.DEBUG:
            print(app, status, err)

        site_path = settings.REAUTHORING_SITE_URL + os.path.splitext(request.path)[0] + '.html'
        return redirect(site_path)

    def _get_file(self, request, mode):
        # TODO: Security issue: this probably allows an attacker to edit any file on disk
        file_path = os.path.join(settings.REAUTHORING_SITE_DIR, request.path[1:].rstrip('/'))
        assert os.path.isfile(file_path)

        return open(file_path, mode)

    def _build_file(self, request):
        """Calls Sphinx build()"""
        source_dir = settings.REAUTHORING_SITE_DIR
        build_dir = settings.REAUTHORING_BUILD_DIR

        return sphinxwrapper.build(source_dir, build_dir, force_all=True)

@csrf_exempt
@login_required
def publish(request):
    assert request.method == 'POST'
    assert settings.REAUTHORING_PUBLISH_SCRIPT is not None

    os.system(settings.REAUTHORING_PUBLISH_SCRIPT)

    return redirect(settings.REAUTHORING_PUBLISH_REDIRECT)
